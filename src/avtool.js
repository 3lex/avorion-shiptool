/**
 * AVORION SHIP UPGRADE TOOL
 *
 * @author  3lex
 * @license MIT
 */
'use strict';

var VERSION = '0.8';

var Ship = {
  xml : null,
  name: null,
}

var $dataContainer  = $('#data-container'),
    $importBtn      = $('#dataImport'),
    $acceptBtn      = $('#dataAccept'),
    $dataName       = $('#dataName'),
    $partsContainer = $('#parts-container'),
    $saveFileName   = $('#saveFileName'),
    $refreshBtn     = $('#dataRefresh'),
    $colorFlushBtn  = $('#toggleColorFlush');

var $blockSelector  = $('<select>', { class: 'block-selector' });




/**
 * if xml data available
 *   clean shipdata
 *   populate shipdata
 *
 * @return {void}
 */
function onDataImport ()
{
  let file = $importBtn[0].files[0];
  if (typeof file === 'object' && file.constructor === File)
  {
    $acceptBtn.removeClass('valid');
    Ship = {
      xml : null,
      name: null,
    };
    let fr = new FileReader();
        fr.readAsText(file);
        fr.onload = function (e) {
          Ship.xml  = new DOMParser().parseFromString(e.target.result, 'text/xml');
          Ship.name = file.name;

          console.info('File:: %s', file.name);
          console.info(`PartsCount:: ${$(Ship.xml).find('block').length }`);

          $acceptBtn.addClass('valid');
          $saveFileName.val(`[Converted] ${file.name}`);
        };
  } else {
    $acceptBtn.removeClass('valid');
  }
}; // onDataImport ()




/**
 * calls onDataRefresh() and saves Ship.xml as File
 *
 * @return {file}
 */
function onDataSave ()
{
  if (!Ship.xml) return;
  onDataRefresh();

  let fileblob = new Blob([(new XMLSerializer()).serializeToString(Ship.xml)], {type: 'text/xml;charset=utf-8'});
  let filename = ($saveFileName.val() != '') ? $saveFileName.val() : Ship.name;
  saveAs(fileblob, filename);
}; // onDataSave ()




/**
 * for each .part-item with data-process=true Ship.xml gets updated with data.
 *
 * @return {[type]} [description]
 */
function onDataRefresh ()
{
  $('.part-item').each((i, part) => {
    if ($(part).data('process'))
    {
      var index     = $(part).data('index'),
          newIndex  = index,
          material  = $(part).data('material'),
          newMaterial = $(part).parent().data('material');

      var $pConvert = $(part).find('.part-item-convert');
      if ($pConvert.hasClass('active')) newIndex = $pConvert.find('.block-selector option[selected="selected"]').val();

      var $pColor  = $(part).find('.part-item-color');

      $(Ship.xml)
        .find(`[index="${index}"][material="${material}"]`)
        .each((i, shipPart) => {
          /**
           * Converting Index & Material Values
           */
          $(shipPart).attr('material', newMaterial);
          $(shipPart).attr('index', (!isBlockOfMaterial(newIndex, newMaterial)) ? BLOCKS[newIndex].transform : newIndex);
          /**
           * Converting Color Values
           */
           if ($colorFlushBtn.parent('.control-btn').hasClass('active') && $('#colorFlushValue').val())
           {
            let light = 'ff';
            let color = $('#colorFlushValue').val();
            $(shipPart).attr('color', 'ff'+color.substr(1));
           }

           if ($pColor.hasClass('active'))
           {
            let light = $pColor.find('.color-light').val();
            let color = $pColor.find('.color-value').val();
            $(shipPart).attr('color', parseInt(light).toString(16) + color.substr(1) );
           }

        });
    }
  });
  onPopulateUi();
  $('#dataRefresh').removeClass('fa-spin');
}; // onDataRefresh ()





/**
 * populate sortable Shippart UI
 *   generates a part item container for each block in xml file
 *   if there's already a similar part on the same material level counter gets raised
 *
 * @return {void}
 */
function onPopulateUi ()
{
  if (!Ship.xml || !$acceptBtn.hasClass('valid')) return;
  $('.part-item').remove();

  $(Ship.xml).find('block').each((i, block) => {
    let id        = $(block).parent().attr('index');
    let index     = block.getAttribute('index');
    let material  = block.getAttribute('material');
    let color     = block.getAttribute('color');

    let $tPart    = $(`.part-item[data-material="${material}"][data-index="${index}"]`);
    let $tList    = $(`.material-list[data-material="${material}"]`);
    if (!$tPart.length)
    {
      /**
       * create new Part Item
       */
      let $part = $('<div>', {
        'class'         : 'part-item',
        'data-ids'      : id,
        'data-index'    : BLOCKS[index] ? index : 0,
        'data-material' : material,
        'data-color'    : color,
        'data-count'    : 1,
        'data-process'  : false,
        })
        .appendTo($tList);
      let $pTitle = $('<span>', {
        'class'         : 'part-item-title',
        'text'          : BLOCKS[index] ? BLOCKS[index].name : BLOCKS[1].name,
        }).appendTo($part);
      let $pCount = $('<span>', {
        'class'         : 'part-item-count',
        'text'          : $part.data('count'),
        })
        .on('click', e => {
          $(e.currentTarget).parent().toggleClass('open');
        })
        .appendTo($part);
      let $pColorContainer = $('<div>', {
        'class'         : 'part-item-color',
        }).appendTo($part);
      let $pColorToggle = $('<span>', {
        'class'         : 'toggle toggle-color',
        'text'          : 'Color',
        })
        .on('click', e => {
          $(e.currentTarget).parent().toggleClass('active');
          $(e.currentTarget).parents('.part-item').addClass('needs-updating').data('process', true);
          $refreshBtn.addClass('fa-spin');
        })
        .appendTo($pColorContainer);
      let $colorValue = $('<input>', {
        'class'         : 'color-value',
        'type'          : 'color',
        'title'         : 'Color Value',
        'value'         : '#'+color.substr(2),
        })
        .appendTo($pColorContainer);
      let $colorLight = $('<input>', {
        'class'         : 'color-light',
        'type'          : 'range',
        'min'           : 25,
        'max'           : 255,
        'step'          : 5,
        'value'         : parseInt(color.substr(0,2), 16),
        'title'         : 'Light Value',
        })
        .appendTo($pColorContainer);
      let $pConvertContainer = $('<div>', {
        'class'         : 'part-item-convert',
        }).appendTo($part);
      let $pConvertToggle = $('<span>', {
        'class'         : 'toggle toggle-convert',
        'text'          : 'Convert Block',
        })
        .on('click', e => {
          $(e.currentTarget).parent().toggleClass('active');
        })
        .appendTo($pConvertContainer);
      let $selector = $blockSelector.clone();
          $selector
            .on('change', e => {
              $(e.currentTarget).parents('.part-item').addClass('needs-updating').data('process', true);
              $refreshBtn.addClass('fa-spin');
              $(e.target).find('option').removeAttr('selected');
              $(e.target.selectedOptions[0]).attr('selected', 'selected');

              if (BLOCKS[$(e.target.selectedOptions[0]).val()].v[$(e.target).parents('.material-list').data('material')] === 1) {
                $(e.currentTarget).parents('.part-item-convert').addClass('valid').removeClass('invalid');
              } else {
                $(e.currentTarget).parents('.part-item-convert').addClass('invalid').removeClass('valid');
              }
            })
            .appendTo($pConvertContainer);
          $selector
            .find(`[value="${index}"]`)
            .each((i, option) => {
              $(option).attr('selected', true);
            });
    } else {
      /**
       * update existing Part Item
       */
      $tPart.data('count', parseInt($tPart.data('count'))+1);
      $tPart.data('ids', $tPart.data('ids')+','+id);
      $tPart.find('.part-item-count').text($tPart.data('count'));
    }
  });
  updateMaterialListHeaders();
  $dataContainer.addClass('file-accepted');
}; // onPopulateUi ()




/**
 * create materials list
 *   generates ul for each material slot
 *   initializes sortable ui
 *
 * @return {void}
 */
function createMaterialsList ()
{
  if (Object.prototype.toString.call(RESSOURCES) !== '[object Array]')
  {
    console.log('[ERROR] missing ressources list file.');
  } else {
    $(RESSOURCES).each((i, material) => {
      let $materialList = $('<div>', {
        'class' : 'material-list',
        'data-material': i,
        'data-material-count': 0,
      }).sortable({
        'connectWith': '.material-list',
        'items': '.part-item',
        'cancel': '.material-list-title',
        'handle': '.part-item-title',
        'revert': true,
        'cursor': 'move',

        activate (e, ui)
        {
          $(e.target).addClass((isBlockOfMaterial($(ui.item[0]).data('index'), $(e.target).data('material'))) ? 'valid' : 'invalid' );
        },
        stop (e, ui)
        {
          $('.material-list').removeClass('valid invalid');
          $(ui.item[0]).data('process', 'true').addClass('needs-updating');
          $refreshBtn.addClass('fa-spin');
        },
        over (e, ui)
        {
          if (!isBlockOfMaterial( $(ui.item[0]).data('index'), $(e.target).data('material')) )
          {
            $(ui.item[0]).addClass('needs-converting');
          } else {
            $(ui.item[0]).removeClass('needs-converting');
          }
        },
        out (e, ui)
        {

        },
      }).disableSelection().appendTo($partsContainer);

      let $title = $('<h4>', {
        'class' : 'material-list-title',
        'text'  : material,
      }).appendTo($materialList);

      let $count = $('<span>', {
        'class' : 'material-list-count',
        'text'  : 0,
      }).appendTo($materialList);
    });
  }
}; // createMaterialsList ()



/**
 * remove file accepted class
 * @return {void}
 */
function onImportNewShip ()
{
  $dataContainer.removeClass('file-accepted');
}; // onImportNewShip ()



/**
 * add file accepted class
 * @return {void}
 */
function onCancelImportNewShip ()
{
  $dataContainer.addClass('file-accepted');
}; // onCancelImportNewShip ()



/**
 * update material list counter
 *
 * @return {void}
 */
function updateMaterialListHeaders ()
{
  $('.material-list').each((i, item) => {
    $(item).data('material-count', 0);
  });

  $('.part-item').each((i, item) => {
    $(item).parent().data(
      'material-count',
      parseInt($(item).data('count') + $(item).parent().data('material-count'))
      );
  });
  $('.material-list-count').each((i, item) => {
    $(item).text( $(item).parent().data('material-count') );
  });

  $acceptBtn.removeClass('is-loading');
} ; // updateMaterialListHeaders ()




/**
 * if 'index' is existent for 'material' returns true
 *
 * @param  {jquery}  index     []
 * @param  {jquery}  material []
 * @return {Boolean}          []
 */
function isBlockOfMaterial (index, material)
{
  return ( BLOCKS[index].v[material] === 1 ) ? true : false;
}; // isBlockOfMaterial ()




/*************************************************************************
 * DOCUMENT READY
 ************************************************************************/
$(() => {
  console.log('AVORION Ship Ugrade Tool | version %s', VERSION);
  createMaterialsList();

  Object.keys(BLOCKS).map((block, i) => {
    let $sel = $('<option>', {
      'text'      : BLOCKS[block].name,
      'value'     : block,
    }).appendTo($blockSelector);
  });

  $('#toggleColorFlush').on('click', e => {
    $(e.target).parents('.control-btn').toggleClass('active');
  });
});


/*************************************************************************
 * POLYFILLS
 ************************************************************************/
if (!String.prototype.splice) {
    /**
     * {JSDoc}
     *
     * The splice() method changes the content of a string by removing a range of
     * characters and/or adding new characters.
     *
     * @this {String}
     * @param {number} start Index at which to start changing the string.
     * @param {number} delCount An integer indicating the number of old chars to remove.
     * @param {string} newSubStr The String that is spliced in.
     * @return {string} A new string with the spliced substring.
     */
    String.prototype.splice = function(start, delCount, newSubStr) {
        return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    };
}

# Avorion Ship Tool

## Installation

Download Repository to a folder of your choice.


bower_components/

  - file-saver/*

  - jquery/*

  - jquery-ui/*

  - font-awesome/*

src/

  - sass/*

  - avtool.js

  - blocklist.js

index.htm

styles.css


Open **index.htm** in a modern browser (chrome, firefox, opera, ...)
and make sure javascript is allowed and scripts not blocked.

## Usage

### 1. Choose File

Push **Choose File** button and find your ship xml file.
If you want to be on the very safe side use a copy of your shipfile.

Press Import Button and Shipdata will show up.

You should see a list of ShipParts listed in their Material slots.
Numbers represent part counts.

![list of shipparts in their material slots](screenshot.png)

### 2. Make Changes

Click on the NameTag for "drag and drop"-ing a ShipPart of your choice (e.g. Thrusters) to another material level.
You will see an orange border, which will indicate that this part is marked for process action.

If a ShipPart is not available on a certain material level, those categories will blur out.
E.g. Titanium Armor Block is not available for naonite level.

You can, nevertheless, drop the ShipPart in every level slot.
An pinkish colored border will indicate that this part will be converted to a different similar block that is available.

You can open the part configs by clicking the part counter once.
To transform parts to a different one (e.g. you want to change "hull windowed" to "hull")
open the part configs list and click "Convert Block" once.
This will also override automatic block converting.

## 3. Colorize
To colorize parts you can either use the "flush" button or on each part individually.

You'll find the flush color button at the top bar marked with a paint brush symbol.
Activate will show a color input to set the flush color. Intensity is always FF.
As soon changes get refreshed flush color will applied to each part, which is marked for process action.
Part Color will override Flush Color.

To colorize each part individually open the part configs list and activate feature by clicking Color.
The range slider represents kind of a light/intensity level avorion is using for colors

### 4. Refresh Changes

**Refresh Changes** button transforms unavailable ShipParts, sets colors and material changes and also refreshes the UI.


### 5. Save

**Save** button saves your new Ship.xml file to your Browser specific Download Folder


## History

version 0.8
  - added feature: colorize part
  - added feature: convert part to other part
  - fixed some css

version 0.7a
  - hotfix: fixed if no file was selected for input throws error
  - fixed some css
